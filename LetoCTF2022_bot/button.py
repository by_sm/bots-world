from telebot import types

markup_user = types.ReplyKeyboardMarkup()
markup_admin = types.ReplyKeyboardMarkup()
item_btn_flag = types.KeyboardButton('Сдать флаг')
item_btn_task = types.KeyboardButton('Показать задание')
item_btn_score = types.KeyboardButton('Показать рейтинг')
item_btn_admin_add_task = types.KeyboardButton('Обновить задание')
item_btn_admin_add_flag = types.KeyboardButton('Обновить флаг')
item_btn_admin_change_solve = types.KeyboardButton('Сбросить все решения')
item_btn_admin_spam = types.KeyboardButton('Массовая рассылка сообщений')
item_btn_admin_stats = types.KeyboardButton('Статистика')

markup_user.row(item_btn_task,item_btn_flag)
markup_user.row(item_btn_score)

markup_admin.row(item_btn_task,item_btn_flag,item_btn_score)
markup_admin.row(item_btn_admin_add_task,item_btn_admin_add_flag,item_btn_admin_change_solve)
markup_admin.row(item_btn_admin_spam,item_btn_admin_stats)