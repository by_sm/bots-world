from curses.ascii import US
from re import sub as subreplace
from time import time, sleep
from telebot import TeleBot
from random import choice
from os import environ
from button import *
from peewee import (AutoField, 
                    TextField, 
                    IntegerField, 
                    BooleanField, 
                    DateTimeField, 
                    Model,
                    SqliteDatabase)

conn = SqliteDatabase('database/letoctf.sqlite')

class BaseModel(Model):
    class Meta:
        database = conn

class Users(BaseModel):
    user_id = AutoField(column_name='UserID')
    name = TextField(column_name='UserName', null=True)
    score = IntegerField(column_name="score", default=0)
    telegram_user_id = TextField(column_name='TGUserID', null=True)
    solve = BooleanField(column_name="Solve", default=False)
    admin = BooleanField(column_name="Admin", default=False)
    timestamp = DateTimeField(column_name="TimeStamp", default=0)

    class Meta:
        table_name = 'Users'

class Task(BaseModel):
    task_id = IntegerField(column_name='TaskID', default=1)
    description = TextField(column_name="TaskDescription", default="Test task")
    points = IntegerField(column_name="TaskPoints", default=100)
    flag = TextField(column_name="TaskFlag", default="Test")

    class Meta:
        table_name = 'Task'

conn.create_tables([Users, Task])
Task.create()
cursor = conn.cursor()

bot = TeleBot(environ['BOT_TOKEN'])

def check(message) -> bool:
    telegram_user_id = message.from_user.id
    try:
        Users.get(Users.telegram_user_id == telegram_user_id)
        return True
    except:
        return False

def is_admin(message) -> bool:
    return True if Users.get(Users.telegram_user_id == message.from_user.id).admin else False

@bot.message_handler(commands=['start'])
def hello(message):
    welcome = '*Добро пожаловать!*'
    name = str(message.from_user.username) if not None else \
        (str(message.from_user.firstname) + str(message.from_user.lastname))
    telegram_user_id = message.from_user.id
    if check(message):
        if is_admin(message):
            bot.send_message(message.chat.id, \
                         welcome, \
                         reply_markup=markup_admin, \
                         parse_mode="Markdown")
        else:
            bot.send_message(message.chat.id, \
                         welcome, \
                         reply_markup=markup_user, \
                         parse_mode="Markdown")
    else:
        Users.create(name=name, telegram_user_id=telegram_user_id)
        bot.send_message(message.chat.id, \
                         "*Регистрация завершена, удачи!*", \
                        reply_markup=markup_user, \
                        parse_mode="Markdown")

@bot.message_handler(regexp="Показать задание")
def task(message):
    if check(message):
        task = Task.get(Task.task_id == 1).description
        if is_admin(message):
            bot.send_message(message.chat.id, \
                         task, \
                         reply_markup=markup_admin,\
                         parse_mode="Markdown")
        else:
            bot.send_message(message.chat.id, \
                            task, \
                            reply_markup=markup_user, \
                            parse_mode="Markdown")
    else:
        bot.send_message(message.chat.id, \
                        'Сперва нажми /start')

@bot.message_handler(regexp="Сдать флаг")
def echo_message(message):
    solve = Users.get(Users.telegram_user_id == message.from_user.id).solve
    if solve == True:
        bot.send_message(message.chat.id, \
                        "*Ты уже успешно сдал это задание :)*", \
                        parse_mode="Markdown")
        return
    else:
        msg = bot.send_message(message.chat.id, \
                              '*Введите флаг:*', \
                              parse_mode="Markdown")
    bot.register_next_step_handler(msg, flag )

def flag(msg):
    if check(msg):
        flag = msg.text
        if flag == Task.get(Task.task_id == 1).flag:
            points_task = Task.get(Task.task_id == 1).points
            score_user = Users.get(Users.telegram_user_id == msg.from_user.id).score
            points = points_task + score_user
            timestamp = Users.get(Users.telegram_user_id == msg.from_user.id).timestamp
            timestamp += time()
            query = Users.update(solve=True, \
                        score=points, \
                        timestamp=timestamp)\
                .where(Users.telegram_user_id == msg.from_user.id)
            query.execute()
            correct = ["*Отлично!*", "*Так держать!*", "*Ииии это верный ответ!*"]
            bot.send_message(msg.chat.id, \
                            choice(correct), \
                            parse_mode="Markdown")
        else:
            bot.send_message(msg.chat.id, \
                            "*Кажется ты ошибся, попробуй еще*", \
                            parse_mode="Markdown")
    else:
        bot.send_message(msg.chat.id, \
                         'Сперва нажми /start')

@bot.message_handler(regexp="Показать рейтинг")
def task(message):
    if check(message):
        query = Users.select()\
                .where(Users.admin != True)\
                .order_by(Users.score.desc(), Users.timestamp.asc())\
                .dicts()\
                .execute()
        top_result = "Топ 10 участников:\n\n"
        count = 0
        length = len(query)
        user_result = ""
        for iter in range(length):
            if count < 10:            
                top_result += f"{str(iter+1)}. {str(query[iter]['name'])} - {str(query[iter]['score'])}\n"
            if str(query[iter]['telegram_user_id']) == str(message.from_user.id):
                user_result += f"Твое место в рейтинге: {str(iter+1)}\n\n"
            count += 1
        result = f"{user_result}{top_result}"
        try:
            bot.send_message(message.chat.id, \
                            result)
        except:
            bot.send_message(message.chat.id, \
                            "*Еще никто ничего не решил :(*", \
                            parse_mode = "Markdown")
    else:
        bot.send_message(message.chat.id, \
                        'Сперва нажми /start')

@bot.message_handler(commands=['admin_panel'])
def admin(message):
    if check(message):
        if is_admin(message):
            bot.send_message(message.chat.id, \
                            "*Добро пожаловать!*",\
                            parse_mode="Markdown",\
                            reply_markup=markup_admin)
        else:
            msg = bot.send_message(message.from_user.id, \
                                '*Введите пароль:*', \
                                parse_mode="Markdown", \
                                reply_markup=markup_user)
            bot.register_next_step_handler(msg, admin_reg )
    else:
        bot.send_message(message.from_user.id, \
                        'Сперва нажми /start')
def admin_reg(msg):
        password_from = msg.text
        if password_from == "ASDewrf4rwfsdr4fkdo0!":
            query = Users.update(admin=True)\
            .where(Users.telegram_user_id == msg.from_user.id)
            query.execute()
            bot.send_message(msg.chat.id, \
                            "*Проверка пройдена, ты добавлен в список админов.*", \
                            parse_mode="Markdown", \
                            reply_markup=markup_admin)
        else:
            bot.send_message(msg.chat.id, \
                            "Ошибочка, давай еще разок", \
                            parse_mode="Markdown", \
                            reply_markup=markup_user)

@bot.message_handler(regexp="Сбросить все решения")
def solve_change(message):
    if check(message):
        if is_admin(message):
            Users.update(solve=False).execute()
            bot.send_message(message.from_user.id, "Complete.")
        else:
            bot.send_message(message.from_user.id, "Nope.")
    else:
        bot.send_message(message.from_user.id, \
                        'Сперва нажми /start')

@bot.message_handler(regexp="Обновить задание")
def update_task(message):
    if check(message):
        if is_admin(message):
            msg = bot.send_message(message.from_user.id, \
                                '*Введите новый текст задания:*', \
                                parse_mode="Markdown", \
                                reply_markup=markup_admin)
            bot.register_next_step_handler(msg, task_update)
        else:
            bot.send_message(message.from_user.id, "Nope.")
    else:
        bot.send_message(message.from_user.id, \
                        'Сперва нажми /start')

def task_update(msg):
    message = msg.html_text
    message = subreplace("(<b>(.*?)<\/b>)", r"*\2*", message)
    message = subreplace("(<s>(.*?)<\/s>)", r"~\2~", message)
    message = subreplace("(<pre>(.*?)<\/pre>)", r"`\2`", message)
    message = subreplace("(<i>(.*?)<\/i>)", r"_\2_", message)
    Task.update(description = message).execute()
    bot.send_message(msg.from_user.id, "Updated.")

@bot.message_handler(regexp="Обновить флаг")
def update_flag(message):
    if check(message):
        if is_admin(message):
            msg = bot.send_message(message.from_user.id, \
                                '*Введите новый флаг:*', \
                                parse_mode="Markdown", \
                                reply_markup=markup_admin)
            bot.register_next_step_handler(msg, flag_update)
        else:
            bot.send_message(message.from_user.id, "Nope.")
    else:
        bot.send_message(message.from_user.id, \
                        'Сперва нажми /start')

def flag_update(msg):
    Task.update(flag = msg.text).execute()
    bot.send_message(msg.from_user.id, "Updated.")

@bot.message_handler(regexp="Массовая рассылка сообщений")
def update_flag(message):
    if check(message):
        if is_admin(message):
            msg = bot.send_message(message.from_user.id, \
                                '*Введите сообщение для рассылки:*', \
                                parse_mode="Markdown", \
                                reply_markup=markup_admin)
            bot.register_next_step_handler(msg, message_spam)
        else:
            bot.send_message(message.from_user.id, "Nope.")
    else:
        bot.send_message(message.from_user.id, \
                        'Сперва нажми /start')

def message_spam(msg):
    telegram_users_list = [i['telegram_user_id'] for i in Users\
                            .select(Users.telegram_user_id)\
                            .dicts()\
                            .execute()]
    for user in telegram_users_list:
        try:
            bot.send_message(user, msg.text)
            open('log/complete.txt', 'a').writelines(f"Suspicious sending message on user {user};\
                                          Message {msg.text};\
                                          Time {time()}\n")
            sleep(0.5)
        except:
            open('log/error.txt', 'a').writelines(f"Fail sending message on user {user};\
                                             Message {msg.text};\
                                             Time {time()}\n")
    bot.send_message(msg.from_user.id, "Complete.")

@bot.message_handler(regexp="Статистика")
def tstats(message):
    if check(message):
        if is_admin(message):
            all = Users.select()\
                .where(Users.admin != True)\
                .dicts()\
                .execute()
            active_users = Users.select()\
                        .where(Users.score > 0, Users.admin != True)\
                        .dicts()\
                        .execute()          
            result = f"*Всего участников:* {len(all)}\n*Участников с ненулевым результатом:* {len(active_users)}"
            bot.send_message(message.from_user.id, \
                            result, \
                            parse_mode="Markdown")
        else:
            bot.send_message(message.from_user.id, \
                            'Nope.')
    else:
        bot.send_message(message.from_user.id, \
                        'Сперва нажми /start')
if __name__=='__main__':
        bot.polling(none_stop=True)