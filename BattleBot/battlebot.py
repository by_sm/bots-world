import telebot, sqlite3
from random import choice
from time import sleep
import config

"""solve 0 - registered user
   solve 1 - user in buttle
   solve 2 - user win
   solve 3 - user lose
"""

bot = telebot.TeleBot(config.token)

conn = sqlite3.connect('Battle.db')
c = conn.cursor()
c.execute("CREATE TABLE IF NOT EXISTS user_battle (id INTEGER PRIMARY KEY, nickname TEXT, solve int, teleg_id)")
conn.commit()
c.close()
conn.close()

@bot.message_handler(commands=['start'])
def hello(message):
    hi = '''
Welcome to crazy CTF battle!
Commands list:
/reg - register you in new Battle
/time - get the time and data the closest battle
/unreg - seriously? You shamefully running away?
/flag - you solve this task?'''
    bot.send_message(message.chat.id, hi)

@bot.message_handler(commands=['reg'])
def register(message):
    row_name = str(message.from_user.first_name) + str (message.from_user.last_name)
    user_id = message.from_user.id
    conn = sqlite3.connect('Battle.db')
    c = conn.cursor()
    c.execute("SELECT COUNT(*) FROM user_battle WHERE nickname='{0}' OR id='{1}'".format(row_name, user_id))
    row = list(c)[0]
    if row[0] == 0:
        c.execute("INSERT INTO user_battle(nickname, teleg_id, solve) values ('{0}', '{1}', '{2}')".format(row_name, user_id, 0))
        bot.send_message(message.chat.id, "Now you registered in CTF Battle!")
        conn.commit()
        c.close()
        conn.close()
    else:
        bot.send_message(message.chat.id, "You id alredy exists")
        conn.commit()
        c.close()
        conn.close()

@bot.message_handler(commands=['flag'])
def flag(message):
    conn = sqlite3.connect("Battle.db")
    c = conn.cursor()

    flag = message.text[6::]

    check = c.execute("SELECT solve FROM user_battle WHERE teleg_id='{}'".format(message.from_user.id)).fetchall()[0][0]

    if check == 0:
        bot.send_message(message.chat.id, "It's not you task :)")

    if check == 1:
        if flag == config.flag:
            bot.send_message(message.chat.id, "WINNER!")
            c.execute("UPDATE user_battle SET solve=2 WHERE teleg_id='{}'".format(message.from_user.id))
            c.execute("UPDATE user_battle SET solve=3 WHERE solve=1")
            conn.commit()
            winner = c.execute("SELECT nickname FROM user_battle where solve=2").fetchall()[0][0]
            userList = c.execute("SELECT teleg_id FROM user_battle").fetchall()
            for i in userList:
                bot.send_message(i[0], "{} is our winner!".format(winner))
        else:
            bot.send_message(message.chat.id, 'No!')

    elif check == 2:
        bot.send_message(message.chat.id, "You alredy solved this task")

    elif check == 3:
        bot.send_message("You lose this task.")
    conn.commit()
    c.close()
    conn.close()

@bot.message_handler(commands=['time'])
def get_battle_time(message):
'''____________________________________
    GET NEW DATE HERE!!!
   ____________________________________'''

    s = 'Every day in 13.00!'
    bot.send_message(message.chat.id, s)

@bot.message_handler(commands=['unreg'])
def unregister(message):
    row_name = str(message.from_user.first_name) + str (message.from_user.last_name)
    user_id = message.from_user.id
    conn = sqlite3.connect('Battle.db')
    c = conn.cursor()
    c.execute("DELETE FROM user_battle WHERE nickname='{0}' OR id='{1}'".format(row_name, user_id))
    bot.send_message(message.chat.id, "Loooser")
    conn.commit()
    c.close()
    conn.close()

def start_battle():
    c.execute("UPDATE user_battle SET solve=0")
    conn = sqlite3.connect("Battle.db")
    c = conn.cursor()
    m = c.execute("SELECT teleg_id from user_battle").fetchall()
    a = []
    for r in m:
        a.append(r[0])
    user1 = choice(a)
    user2 = choice(a)
    if user1 == user2:
        user2 = choice(a)
    else:
        conn = sqlite3.connect('Battle.db')
        c = conn.cursor()
        user_one = c.execute("SELECT nickname from user_battle where teleg_id='{}'".format(user1)).fetchall()
        for i in user_one:
            m = i[0]
        user_two = c.execute("SELECT nickname from user_battle where teleg_id='{}'".format(user2)).fetchall()
        for i in user_two:
            f = i[0]

        c.execute("UPDATE user_battle SET solve=1 where teleg_id='{}'".format(user1))
        c.execute("UPDATE user_battle SET solve=1 where teleg_id='{}'".format(user2))

        say = 'And our \'winners\' are:\n {0} and {1}!\n Good luck!'.format(m, f)
        userList = c.execute("SELECT teleg_id from user_battle").fetchall()
        for i in userList:
            bot.send_message(i[0], say)
        bot.send_message(user1, 'test task')
        bot.send_message(user2, 'test task')

        conn.commit()
        c.close()
        conn.close()

@bot.message_handler(commands=['timetostart'])
def begin(message):
    bot.send_message(message.chat.id, 'starting...')
    start_battle()

if __name__ == "__main__":
    try:
        bot.polling(none_stop=True)
    except:
        sleep(10)
