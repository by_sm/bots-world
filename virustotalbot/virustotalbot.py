import telebot
from requests import get, post
from time import sleep
from zipfile import ZipFile

vt_token = ''
bot_token = ''
bot = telebot.TeleBot(bot_token)

def get_vt(hash, message):
    try:
        answer = ''
        params = {'apikey': vt_token, 'resource': hash}
        headers = {"Accept-Encoding": "gzip, deflate",
          "User-Agent" : "gzip,  scanbot"}
        while True:
            response = get('https://www.virustotal.com/vtapi/v2/file/report',params=params, headers=headers)
            json_response = response.json()
            if json_response['response_code'] == -2:
                sleep(30)
            else:
                for av in json_response['scans']:
                    if str(json_response['scans'][av]['detected']) != 'False':
                        answer += av + ' ' + str(json_response['scans'][av]['detected'])
                score = json_response['positives']
                hash = json_response['sha1']
                answer = "Total Score: " + str(score) + '\n' + "Hash " + str(hash)
                bot.send_message(message.chat.id, answer)
                return
    except Exception as e:
        bot.send_message(message.chat.id, e)

def post_vt(file, message):
    try:
        filename = message.document.file_name
        if message.caption == 'enczip':
            with ZipFile('files/'+message.document.file_name) as zf:
                zf.extractall(pwd=b'malware', path='/Users/by_sm/Script/all/Bots/virustotalbot/files/')
                filename = message.document.file_name[:len(message.document.file_name )-4]
        bot.send_message(message.chat.id, 'Send to VirusTotal... Please wait a few minutes...')
        params = {'apikey': vt_token}
        files = {'file': ('files/'+filename, open('files/'+filename, 'rb'))}
        response = post('https://www.virustotal.com/vtapi/v2/file/scan', files=files, params=params)
        json_response = response.json()
        sleep(10)
        get_vt(json_response['resource'], message)
    except:
        bot.send_message(message.chat.id, 'Error!')

@bot.message_handler(commands=['start'])
def start(message):
    hello = 'test'
    bot.send_message(message.chat.id, hello)

@bot.message_handler(content_types=["document"])
def doc(message):
    file_info = bot.get_file(message.document.file_id)
    download = bot.download_file(file_info.file_path)
    src = '/Users/by_sm/Script/all/Bots/virustotalbot/files/'+message.document.file_name
    with open(src, 'wb') as new_file:
       new_file.write(download)
    post_vt(message.document.file_name, message)

bot.polling()
