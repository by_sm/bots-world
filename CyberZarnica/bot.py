import telebot
import sqlite3
import string
import config

bot = telebot.TeleBot('config.token')

conn = sqlite3.connect('CyberZadnica.db')
c = conn.cursor()
c.execute("CREATE TABLE IF NOT EXISTS user (id INTEGER PRIMARY KEY, number_command INT not NULL, user_id TEXT, task_1 INTEGER, task_2 INTEGER, task_3 INTEGER, task_4 INTEGER, task_5 INTEGER, task_6 INTEGER, task_7 INTEGER)")
conn.commit()
c.close()
conn.close()


@bot.message_handler(commands=['start'])
def hello(message):
    welcome = '''
Добро пожаловать на КиберЗарницу 2017!
Я буду принимать у вас финальные флаги, не забывайте мне их скидывать ;)
Для начала, введите команду /reg и укажите номер своей команды, например:
/reg 1
Учтите, что общаться со мной должен только ОДИН из участников команды!

Для отправки любого флага введите команду /flag и укажите полученную строку, например:
/flag ghre124vds

Удачи!
'''
    bot.send_message(message.chat.id, welcome)


@bot.message_handler(commands=['reg'])
def help(message):
    user_id = str(message.from_user.id)
    row_number_command = str(message.text)
    num_c = row_number_command[5::]
    conn = sqlite3.connect('CyberZadnica.db')

    if len(num_c) == 0:
        bot.send_message(message.chat.id, "Введите номер команды!")
    else:
        c = conn.cursor()
        c.execute("SELECT COUNT(*) FROM user WHERE number_command='{0}' OR user_id='{1}'".format(num_c, user_id))
        row = list(c)[0]
        print(row[0])

        if row[0] != 0:
            bot.send_message(message.chat.id, "Ваша команда уже зарегестрирована!")
            conn.close()
        else:
            c.execute("INSERT INTO user (number_command, user_id) VALUES ('%s', '%s')" % (int(num_c), str(user_id)))
            conn.commit()
            conn.close()
            bot.send_message(message.chat.id, "Зарегестрировал!")

@bot.message_handler(commands=['flag'])
def flag(message):
    user_id = str(message.from_user.id)
    row_flag = str(message.text)
    conn = sqlite3.connect('CyberZadnica.db')
    c = conn.cursor()
    c.execute("SELECT count(*) from user WHERE user_id='{}'".format(user_id))
    row = list(c)[0]
    #print(row[0])
    if row[0] != 0:
        flag = str(row_flag[6::])
        flags=['1', '2', '3', '4', '5']
        if flag in flags:
            task="task_"+str(flag)
            print (task)
            sql=("SELECT " +task+" FROM user WHERE user_id='{}'".format(user_id))
            c.execute(sql)
            print ("!",sql)
            if (c.fetchall()[0][0]==1):
                bot.send_message(message.chat.id, "NO!")
                c.close()
                conn.close()
            else:
                bot.send_message(message.chat.id, "OK")
                sql = "UPDATE user SET " +task+"='{}' WHERE user_id='{}'".format(1, user_id)
                c.execute(sql)
                conn.commit()
                c.close()
                conn.close()
        else:
            bot.send_message(message.chat.id, "NO!")
            c.close()
            conn.close()
    else:
        bot.send_message(message.chat.id, "Вы не зарегестрированы!")

if __name__=='__main__':
        bot.polling(none_stop=True)

