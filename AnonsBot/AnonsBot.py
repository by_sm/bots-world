﻿# -*- coding: utf-8 -*-

import telebot, config, pywapi, time, emoji, requests, json, datetime, urllib.requests, re
from datetime import timedelta

bot = telebot.TeleBot(config.token)


@bot.message_handler(commands=['start'])
def handle_text(message):
    hi = 'Привет, я тут типо местной секретутки. Жмакни /help, мож че новое узнаешь'
    bot.send_message(message.chat.id, hi)


@bot.message_handler(commands=['help'])
def handle_text(message):
    hell = hell = '''Расписания занятий, предстоящих CTF и прочее.\n
Список доступных команд:\n
/links - полезные ссылки\n
/calendar - три ближайших CTF]\n
/weather - Погода на данный момент\n
/event - ближайшие онлайн соревнования в которых участвует команда\n
/schedule - расписание тренировок/занятий'''
    bot.send_message(message.chat.id, hell)


@bot.message_handler(commands=['schedule'])
def handle_text(message):
    infos = open('info.txt', 'r').read()
    bot.send_message(message.chat.id, infos, parse_mode="Markdown")


@bot.message_handler(commands=['links'])
def handle_text(message):
    links = open('links.txt', 'r').read()
    bot.send_message(message.chat.id, links, parse_mode="Markdown")


@bot.message_handler(commands=['calendar'])
def handle_text(message):
    now = time.time()
    past = now + 3600 * 24 * 30
    api = "https://ctftime.org/api/v1/events/?limit=%d&start=%d&finish=%d" % (3, now, past)
    r = requests.get(api)
    results = json.loads(r.content.decode("utf-8"))
    for result in results:
        msg = ''
        msg += '\nНазвание: %s ' % result['title']
        msg += '\nCайт: %s ' % result['url']
        msg += '\nФормат: %s ' % result['format']
        start = datetime.datetime.strptime(result['start'], "%Y-%m-%dT%H:%M:%S+00:00")
        start_2 = timedelta(days=0, hours=6)
        start_3 = str(start + start_2)
        s = len(start_3)
        s = s - 3
        m = start_3[:s]
        msg += '\nНачало: ' + m
        start1 = datetime.datetime.strptime(result['finish'], "%Y-%m-%dT%H:%M:%S+00:00")
        start_12 = timedelta(days=0, hours=6, seconds=0)
        start_13 = str(start1 + start_12)
        s1 = len(start_13)
        s1 = s1 - 3
        m1 = start_13[:s1]
        msg += '\nКонец:  ' + m1
        bot.send_message(message.chat.id, msg, parse_mode="Markdown", disable_web_page_preview=True)


@bot.message_handler(commands=['event'])
def handle_text2(message):
    event = open('event.txt', 'r').read()
    bot.send_message(message.chat.id, event)


@bot.message_handler(commands=['weather'])
def handle_text(message):
    city = str(message.text[9::])
    if city == '':
        city = 'Novosibirsk'
    data = urllib.urlopen('http://wxdata.weather.com/wxdata/search/search?where={}'.format(city)).read()
    regexp = re.compile(r'(?<=<loc id=")(.{8})(?=" type)')
    result = re.search(regexp, data)
    if result is None:
        bot.send_message(message.chat.id, 'Не нашел я такой город, прости')
        return
    cityCode = result.group(0)
    hi = Weather_and_time_start(cityCode)
    bot.send_message(message.chat.id, hi)


def Weather_and_time_start(cityCode):
    Month = str(time.localtime()[1])
    x = int(Month)
    Mes = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября',
           'декабря']
    m = x - 1
    day = str(time.localtime()[2])
    hour = str(time.localtime()[3])
    minut = str(time.localtime()[4])

    weather_com_result = pywapi.get_weather_from_weather_com(cityCode)
    if weather_com_result['current_conditions']['text'].lower() == 'fair':
        v1 = ' тепло'
    elif weather_com_result['current_conditions']['text'].lower() == 'cloudy' or 'mostly cloudy':
        v1 = ' Облачно'
    elif weather_com_result['current_conditions']['text'].lower() == 'partly cloudy':
        v1 = ' переменная облачность'
    elif weather_com_result['current_conditions']['text'].lower() == 'clear':
        v1 = ' солнечно'
    else:
        v1 = "It is " + weather_com_result['current_conditions']['text'].lower() + " and " + \
             weather_com_result['current_conditions'][
                 'temperature'] + "°C now in " + message.text + "." + "\n" + "Feels like " + \
             weather_com_result['current_conditions']['feels_like'] + "°C. \n" + "Last update - " + \
             weather_com_result['current_conditions']['last_updated']

    Pogoda = weather_com_result['current_conditions']['temperature'].lower() + " °C, " + v1 + '.'
    h = "Погода на " + day + ' ' + Mes[m] + "\nНа улице " + Pogoda
    return h


if __name__ == '__main__':
    bot.polling(none_stop=True)
