var TB = require('node-telegram-bot-api')
var CronJob = require('cron').CronJob;
var request = require('request');
var weather = require('openweather-apis');

var token = ''
var bot = new TB(token, {polling: true})


bot.onText(/\/start/, function(msg) {
  var name = msg.from.username;
  var userId = msg.from.id;
  console.log(name)
  bot.sendMessage(userId, 'Ну привет, ' + name)
});

bot.onText(/\/напомни (.+) в (.+)/, function(msg, match) {
  var notes = []
  var userId = msg.from.id;
  var text = match[1];
  var time = match[2];

  notes.push( {'uid':userId, 'time':time, 'text':text});

  bot.sendMessage(userId, 'А сам запомнить не мог, да?')
  new CronJob('* * * * * *', function() {
  for (var i = 0; i < notes.length; i++){
    var curDate = new Date().getHours() + ':' + new Date().getMinutes();
      if ( notes[i]['time'] === curDate ) {
      bot.sendMessage(notes[i]['uid'], 'Спорим забыл? '+ notes[i]['text']);
      notes.pop(i,1); }}
    }, null, true, 'Asia/Novosibirsk');
  });

bot.onText(/\/погода (.+)/, function(msg, match) {
  var userId = msg.from.id;
  var city = match[1];
  console.log(match[1])
  weather.setLang('ru');
  weather.setCity(city);
  weather.setUnits('metric');
  weather.setAPPID('03400df782f6403045965a78287b3ba9');
  weather.getTemperature(function(err, temp){
      var t = temp;
      weather.getPressure(function(err, press){
        var p = press;
        weather.getHumidity(function(err, hum){
          var h = hum;
  bot.sendMessage(userId, 'Температура: ' + t + '\n' + 'Давление: ' + p + '\n' + 'Влажность: ' + h)
  });});});});
