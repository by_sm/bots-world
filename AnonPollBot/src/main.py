from telebot import TeleBot
from os import environ
from uuid import uuid4
from button import *
from peewee import (AutoField, 
                    TextField, 
                    IntegerField, 
                    BooleanField, 
                    Model,
                    SqliteDatabase)

conn = SqliteDatabase('database/poll.sqlite')

class BaseModel(Model):
    class Meta:
        database = conn

class Users(BaseModel):
    uid = AutoField(column_name='UserID')
    name = TextField(column_name='UserName', null=True)
    telegram_user_id = TextField(column_name='TGUserID', null=True)
    is_vote = BooleanField(column_name="IsVote", default=False)
    is_verificated = BooleanField(column_name="IsVerificated", default=False)

    class Meta:
        table_name = 'Users'

class Participant(BaseModel):
    count_participant = IntegerField(column_name='ParticipantCount', default=0)
    name_participant = TextField(column_name='ParticipantName')

    class Meta:
        table_name = 'Participant'

class Tokens(BaseModel):
    uuid = TextField(column_name='uuid')

    class Meta:
        table_name = 'Tokens'

conn.create_tables([Users, Participant, Tokens])
cursor = conn.cursor()

#Remove all old tokens, create new
Tokens.delete().execute()
tokens = [str(uuid4()) for token in range(60)]
for token in tokens:
    Tokens.create(uuid=token)
print(tokens)

#Update users, set is_vote -> False to all
Users.update(is_vote=False)

#Create participant
Participant.create(name_participant = "Анисин Александр Александрович")
Participant.create(name_participant = "Против всех")

bot = TeleBot(environ['BOT_TOKEN'])

def check(telegram_user_id: str) -> bool:
    try:
        Users.get(Users.telegram_user_id == telegram_user_id)
        return True
    except:
        return False

def is_vote(telegram_user_id: str) -> bool:
    return Users.get(Users.telegram_user_id == telegram_user_id).is_vote
    
def validate_token(token: str) -> bool:
    try:
        obj = Tokens.get(Tokens.uuid == token)
        obj.delete_instance()
        return True
    except:
        return False

@bot.message_handler(commands=['start'])
def start(message):
    telegram_user_id = message.from_user.id
    if not check(telegram_user_id):
        msg = bot.send_message(message.chat.id,\
                              '*Введите токен:*',
                              parse_mode="Markdown")
        bot.register_next_step_handler(msg, register)
    elif is_vote(telegram_user_id):
        bot.send_message(message.chat.id,\
                        'Ты уже голосовал.')   
    else:
        bot.send_message(message.from_user.id,\
                        '*Для начала голосования нажмите соответствующую кнопку*',
                        parse_mode="Markdown",
                        reply_markup=markup_start)    

def register(msg):
    telegram_user_id = msg.from_user.id
    token = msg.text
    name = str(msg.from_user.username) if not None else \
              (str(message.from_user.firstname) + str(message.from_user.lastname))    
    if validate_token(token):
        Users.create(name=name, telegram_user_id=telegram_user_id, is_verificated=True)
        bot.send_message(msg.chat.id,\
                        '*Для начала голосования нажмите соответствующую кнопку*',
                        parse_mode="Markdown",
                        reply_markup=markup_start)
    else:
        msg = bot.send_message(msg.chat.id,\
                              'Неверный токен, повторите попытку')
        bot.register_next_step_handler(msg, register)

@bot.message_handler(regexp="Начать голосование")
def poll(message):
    telegram_user_id = str(message.from_user.id)
    if check(telegram_user_id):
        print(is_vote(telegram_user_id))
        if not is_vote(telegram_user_id):
            msg = bot.send_message(message.chat.id,\
                                '*Внимание! После выбора изменить голос будет нельзя.*',
                                parse_mode="Markdown",
                                reply_markup=markup_user)
            bot.register_next_step_handler(msg, vote)
        else:
            bot.send_message(message.chat.id,\
                            'Ты уже голосовал')

def vote(msg):
    vote = msg.text
    try:
        Participant.get(Participant.name_participant == vote)
        Users.update(is_vote = True)\
             .where(Users.telegram_user_id == msg.from_user.id)\
             .execute()
        Participant.update(count_participant = Participant.count_participant + 1)\
                   .where(Participant.name_participant == vote)\
                   .execute()
        bot.send_message(msg.chat.id,\
                        'Ваш голос учтен.')
    except:
        msg = bot.send_message(msg.chat.id,\
                        'Выберите из доступных вариантов',
                        reply_markup=markup_user)
        bot.register_next_step_handler(msg, vote)

@bot.message_handler(commands=['statistic'])
def stat(message):
    query = Participant.select()\
                       .dicts()\
                       .execute()
    result = ""
    for _ in query:
        result += f"{_['name_participant']} : {_['count_participant']}\n"
    bot.send_message(message.chat.id, result)

if __name__=='__main__':
        bot.polling(none_stop=True)