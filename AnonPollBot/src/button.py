from telebot import types

markup_user = types.ReplyKeyboardMarkup()
markup_start = types.ReplyKeyboardMarkup()

item_btn_participant1 = types.KeyboardButton('Анисин Александр Александрович')
item_btn_participant2= types.KeyboardButton('Против всех')
item_btn_start = types.KeyboardButton('Начать голосование')

markup_user.row(item_btn_participant1)
markup_user.row(item_btn_participant2)
markup_start.row(item_btn_start)