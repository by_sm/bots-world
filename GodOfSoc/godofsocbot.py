import telebot, config, dbworker
from random import choice

bot = telebot.TeleBot(config.api_token)

falseAnswer = ['Неа, не угадал', 'Ошибочка', 'Я бы сказал, что ты был близок... Но нет', 'Моя бабушка и то лучше в логи смотрит!', 'Нууу... Нет', 'Еще попыточку?', 'Ты угада... А не, смотри внимательней']

@bot.message_handler(commands=['start', 'help'])
def start(message):
    state = dbworker.get_info(message.chat.id)
    print("state" + str(state))

    hello ="""Привет стажер!
У нас тут на днях произошел инцидент в Security Operation Center (SOC), нужно разобраться.
Давай так, я даю тебе доступ до более-менее почищенных логов, а ты мне расскажешь что там произошло?
Ну а если ответишь на все вопросы - с меня флаг :)
http://81.23.10.193:4000 auditor:qwerty123
Для начала назови мне имя  хоста, который был атакован.

Просто отсылай мне сообщения, без всяких '/'!

P.S. Не надо пытаться атаковать инфраструктуру :)"""
    bot.send_message(message.chat.id, hello)
    dbworker.set_state(message.chat.id, config.statistisc.S_HOSTNAME.value)

#@bot.message_handler(commands=['godmode'])
#def iamagod(message):
    #state =

@bot.message_handler(func=lambda message: dbworker.get_info(message.chat.id) == config.statistisc.S_HOSTNAME.value)
def check_answer_1(message):
    mes = """Значит целью был наш админ. Ну что-же, давай посмотрим дальше.
Теперь мне интересно узнать внутренний IP адрес атакующего"""
    answer = message.text.upper()
    print(answer)
    if answer == 'WIN-LJT3U4A175S':
        bot.send_message(message.chat.id, mes)
        dbworker.set_state(message.chat.id, config.statistisc.S_IP.value)
    else:
        bot.send_message(message.chat.id, choice(falseAnswer))

@bot.message_handler(func=lambda message: dbworker.get_info(message.chat.id) == config.statistisc.S_IP.value)
def check_answer_2(message):
    answer = message.text
    if answer == '192.168.1.8':
        mes = """А ты хорош! Окей, мы выяснили самые основные моменты. Теперь скажи мне, какими средствами этот хулиган проводил разведку?"""
        bot.send_message(message.chat.id, mes)
        dbworker.set_state(message.chat.id, config.statistisc.S_SOFT.value)
    else:
        bot.send_message(message.chat.id, choice(falseAnswer))

@bot.message_handler(func=lambda message: dbworker.get_info(message.chat.id) == config.statistisc.S_SOFT.value)
def check_answer_3(message):
    answer = message.text.upper()
    if answer == 'NMAP':
        mes = 'А название атаки, которую он совершил, ты уже нашел?'
        bot.send_message(message.chat.id, mes)
        dbworker.set_state(message.chat.id, config.statistisc.S_EXPLOIT.value)
    else:
        bot.send_message(message.chat.id, choice(falseAnswer))

@bot.message_handler(func=lambda message: dbworker.get_info(message.chat.id) == config.statistisc.S_EXPLOIT.value)
def check_answer_4(message):
    answer = message.text.upper()
    if answer == 'ETERNALBLUE':
        mes = """Банально, да? Посмотри что он делал дальше. Меня интересует имя вредоносного файла, который он исполнил"""
        bot.send_message(message.chat.id, mes)
        dbworker.set_state(message.chat.id, config.statistisc.S_FILENAME.value)
    else:
        bot.send_message(message.chat.id, choice(falseAnswer))

@bot.message_handler(func=lambda message: dbworker.get_info(message.chat.id) == config.statistisc.S_FILENAME.value)
def check_answer_5(message):
    answer = message.text.upper()
    if answer == 'MICROSOFTUPDATE-4023.EXE':
        mes = """Слушай, мне кажется, или он не очень умный? Отследим куда отсылаются запросы? Мне интересен домен вида example.com"""
        bot.send_message(message.chat.id, mes)
        dbworker.set_state(message.chat.id, config.statistisc.S_MALICIOUSSITE.value)
    else:
        bot.send_message(message.chat.id, choice(falseAnswer))

@bot.message_handler(func=lambda message: dbworker.get_info(message.chat.id) == config.statistisc.S_MALICIOUSSITE.value)
def check_answer_6(message):
    answer = message.text.upper()
    if answer == 'PASTEBIN.COM':
        mes = """Серьезно? Окей, а название файла, который он воровал ты знаешь?"""
        bot.send_message(message.chat.id, mes)
        dbworker.set_state(message.chat.id, config.statistisc.S_CRACKFILENAME.value)
    else:
        bot.send_message(message.chat.id, choice(falseAnswer))

@bot.message_handler(func=lambda message: dbworker.get_info(message.chat.id) == config.statistisc.S_CRACKFILENAME.value)
def check_answer_7(message):
    answer = message.text.upper()
    if answer == 'LOGIN DATA' or answer == "LOGINDATA":
        mes = """Мда, я ожидал чего нибудь поинтереснее школьника-малварьщика. Ладно, пришло время сделать ему ата-та. Достанешь мне логин-пароль", которые он использовал? Пришли их в виде login:pass"""
        bot.send_message(message.chat.id, mes)
        dbworker.set_state(message.chat.id, config.statistisc.S_LOGINPASSWORD.value)
    else:
        bot.send_message(message.chat.id, choice(falseAnswer))

@bot.message_handler(func=lambda message: dbworker.get_info(message.chat.id) == config.statistisc.S_LOGINPASSWORD.value)
def check_answer_8(message):
    answer = message.text
    if answer == 'tster:zxc123!@#':
        mes = """Поздравляю! Инцидент решен, держи честно заслуженный флаг! letoctf{j@$t_@|\|0th3er_$0c_d@y}"""
        bot.send_message(message.chat.id, mes)
    else:
        bot.send_message(message.chat.id, choice(falseAnswer))

if __name__ == "__main__":
    bot.polling(none_stop=True)
