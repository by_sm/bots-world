from enum import Enum

api_token = ''
db_file = "socdb.vdb"

class statistisc(Enum):
    S_START = b"0"  #start
    S_HOSTNAME = b"1" #hostname
    S_IP = b"2" #source ip
    S_SOFT = b"3" #using soft-scanner
    S_EXPLOIT = b"4" #external exploit
    S_FILENAME = b"5" #malicious files
    S_MALICIOUSSITE = b"6" #site to request
    S_CRACKFILENAME = b"7" #stolen file
    S_LOGINPASSWORD = b"8" #credentials
