import telebot
import sqlite3
import string
import config

flag_first = config.flag1

bot = telebot.TeleBot(config.token)

conn = sqlite3.connect('Task.db')
c = conn.cursor()
c.execute("CREATE TABLE IF NOT EXISTS user (id INTEGER PRIMARY KEY, nickname TEXT not NULL, score int, teleg_id, solve TEXT)")
conn.commit()
c.close()
conn.close()


@bot.message_handler(commands=['start'])
def hello(message):
    welcome = '''
Welcome to new task-bot!
/task - just get task!
/second_task - if you need more tasks :)
/flag <flag> - if you ready to send a response'''
    bot.send_message(message.chat.id, welcome)
    row_name = str(message.from_user.username)
    user_id = message.from_user.id
    conn = sqlite3.connect('Task.db')
    c = conn.cursor()
    c.execute("SELECT COUNT(*) FROM user WHERE nickname='{0}' OR id='{1}'".format(row_name, user_id))
    row = list(c)[0]
    if row[0] == 0:
        c.execute("INSERT INTO user(nickname, teleg_id, score, solve ) values ('{0}', '{1}', '{2}', '{3}')".format(row_name, user_id, 0, 'no'))
        bot.send_message(message.chat.id, "Now you registered!")
        conn.commit()
        c.close()
        conn.close()
    else:
        bot.send_message(message.chat.id, "You id alredy exists")
        conn.commit()
        c.close()
        conn.close()


@bot.message_handler(commands=['help'])
def help(message):
    welcome = '''
/task - just get the task!
/flag <flag> - if you ready to send a response
/score - who is winners?'''
    bot.send_message(message.chat.id, welcome)


@bot.message_handler(commands=['task'])
def get_task(message):
    #try:
    row_name = str(message.from_user.username)
    user_id = message.from_user.id
    conn = sqlite3.connect('Task.db')
    c = conn.cursor()
    c.execute("SELECT COUNT(*) FROM user WHERE nickname='{0}' OR id='{1}'".format(row_name, user_id))
    row = list(c)[0]
    if row[0] != 0:
        f = open('task.txt', 'r')
        task = f.read().encode('utf-8')
        bot.send_message(message.chat.id, task)
        doc = open('enc', 'rb')
        bot.send_document(message.chat.id, doc)
#except:
#            bot.send_message(message.chat.id, 'Something wrong...')


@bot.message_handler(commands=['flag'])
def flag(message):
    try:
        row = message.text
        user_id = message.from_user.id
        flag_user = str(row[6::])
        if flag_user == flag_first:
            conn = sqlite3.connect("Task.db")
            c = conn.cursor()
            score_user = c.execute("SELECT score FROM user WHERE teleg_id='{}'".format(user_id)).fetchall()
            solve = c.execute("SELECT solve FROM user WHERE teleg_id='{}'".format(user_id)).fetchall()
            for s in solve:
                solve = s
                if s[0] == 'yes':
                    bot.send_message(message.chat.id, 'You have already solved this task!')
                else:
                    for i in score_user[0]:
                        score_user = i
                    score_user_new = int(score_user) + 100
                    c.execute("UPDATE user SET score='{1}' WHERE teleg_id='{0}'".format(user_id, score_user_new))
                    c.execute("UPDATE user SET solve='yes' WHERE teleg_id='{0}'".format(user_id))
                    conn.commit()
                    c.close()
                    conn.close()
                    bot.send_message(message.chat.id, "Excellent!")
        else:
            bot.send_message(message.chat.id, "Wrong!")
    except:
            bot.send_message(message.chat.id, 'Something wrong...')


@bot.message_handler(commands=['score'])
def score(message):
    conn = sqlite3.connect("Task.db")
    c = conn.cursor()
    sc = c.execute("SELECT nickname,score from user").fetchall()
    spisok = ''
    for i in sc:
        spisok += str(str(i[0]) + ' ' + str(i[1]) + '\n')
    bot.send_message(message.chat.id, spisok)


if __name__=='__main__':
        bot.polling(none_stop=True)
